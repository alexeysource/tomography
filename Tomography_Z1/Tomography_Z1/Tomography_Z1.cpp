﻿#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <omp.h>


using namespace cv;
using namespace std;

//---MAKE LINE FOR S AND THETTA
Mat computeLineRot(Mat imgzoom, int S, int thetta, int centerPov)
{
	imgzoom = Mat::zeros(imgzoom.size(), CV_64FC1);
	line(imgzoom, Point(0, S), Point(imgzoom.cols, S), Scalar(1,1,1));
	Mat rot = getRotationMatrix2D(Point(centerPov, centerPov), thetta + 90, 1);
	warpAffine(imgzoom, imgzoom, rot, imgzoom.size());
	return imgzoom;
}


//---KAZF METHOD
void Mkazf(double* a, double* b, double* x, int nn, int ny)
{
	// nn - количество неизвестных;  ny - количество уравнений
	float eps = 1.e-2f;
	//float s;
	int i, j, k;
	float s1, s2, fa1, t;
	float* x1;
	int status = 0;
	x1 = new float[nn];



	x[0] = 0.5f;
	for (i = 1; i < nn; i++)  x[i] = 0.f;

	s1 = s2 = 1.f;
	while (s1 > eps* s2)
	{
		for (i = 0; i < nn; i++) x1[i] = x[i];

		for (i = 0; i < ny; i++)
		{
			s1 = 0.0;
			s2 = 0.0;
			for (j = 0; j < nn; j++)
			{
				fa1 = a[i * nn + j];
				s1 += fa1 * x[j];
				s2 += fa1 * fa1;
			}
			if (s2 != 0)
			{
			t = (b[i] - s1) / s2;
			for (k = 0; k < nn; k++)    x[k] += a[i * nn + k] * t;
			}		
		}
		s1 = 0.0;
		s2 = 0.0;
		for (i = 0; i < nn; i++)
		{
			s1 += (x[i] - x1[i]) * (x[i] - x1[i]);
			s2 += x[i] * x[i];
		}
		s1 = (float)sqrt(s1);
		s2 = (float)sqrt(s2);
	
		cout << ++status << "  s1: " << s1 << "  eps*s2: " << eps * s2 << endl;
	}
	delete[] x1;
}




int main()
{
	cout << " Enter 0 for tranformation, or other for test " << endl;
	int p = 0;
	cin >> p;

	if (p == 0)
	{
		
		//====DON'T PAY ATTENTION ON THIS BAD CODE, PLS :)
		//====IT WILL BE MORE UNDERSTANDABLE FROM 140 ROW

		Mat imagesrc = Mat::zeros(50, 50, CV_64FC1);

		//Mat imagesrc = imread("Lenna50.png", IMREAD_GRAYSCALE);
		
		
		//Our test image is white rectangle on black background

		//rectangle(imagesrc, Rect(30, 30, 40, 40), Scalar(255), -1);
		//rectangle(imagesrc, Rect(0, 0, 50, 50), Scalar(255), -1);
		//rectangle(imagesrc, Rect(0, 0, 25, 50), Scalar(255), -1);
		
		rectangle(imagesrc, Rect(10, 15, 30, 20), Scalar(255), -1); 		
		circle(imagesrc, Point(25, 35), 10, Scalar(255), -1);
		



		//Convert Image to black-white (if it colour)
		Mat imageConverted;
		imagesrc.convertTo(imageConverted, CV_64FC1);
		cv::normalize(imageConverted, imageConverted, 0, 1, NORM_MINMAX);
		cv::imshow("imageConverted", imageConverted);

		int step_thetta = 1;
		int sino_heigih = 180 / step_thetta; // Sinigram's height depends from thetta step

		double w = imagesrc.cols;
		double h = imagesrc.rows;

		std::cout << "Image src w, h:  " << w << "x" << h << endl;

		int d = int(sqrt(w * w + h * h));
		int dw = int((d - w) / 2);
		int dh = int((d - h) / 2);

		Mat imagezero = Mat::zeros(imagesrc.size(), CV_64FC1);

		Mat zoom_img;
		cv::copyMakeBorder(imagezero, zoom_img, dh, dh, dw, dw, BORDER_CONSTANT);
		Mat imgzoom2 = Mat::zeros(zoom_img.size(), CV_64FC1);
		int centerPov = imgzoom2.cols / 2;

		Mat out_img = Mat::zeros(sino_heigih, d, CV_64FC1);

		Mat Line;
		Mat croppedImage = Mat::zeros(imagesrc.size(), CV_64FC1);

		//Creation 1d mat with "pictures with lines"
		Mat matLines1d = Mat::zeros(1, sino_heigih * d * imagesrc.rows * imagesrc.cols, CV_64FC1);


		Mat LineIMage = Mat::zeros(imagesrc.size(), CV_64FC1);


		//================READ COMMENTS FROM HERE===============

		int counter = 0;
#pragma parallel for
		for (int thetta = 0; thetta < sino_heigih; thetta += step_thetta)
		{
			for (int S = 0; S < d; S++)
			{
				//====Creation picture with line and "mul" it to src_img
				Line = computeLineRot(imgzoom2, S, thetta, centerPov);
				croppedImage = Line(Rect(dw, dh, w, h));

				//====Creation 1d array with "pictures with lines"
				int k = 0;
				for (int i = 0; i < croppedImage.rows; i++)
				{
					for (int j = 0; j < croppedImage.cols; j++)
					{				
						matLines1d.at<double>(0, counter * croppedImage.rows * croppedImage.cols + k) = croppedImage.at<double>(i, j);
						k++;
					}
				}		
				
				//====Creation sinogramm
				croppedImage = croppedImage.mul(imageConverted);			
				out_img.at<double>(thetta, S) = double(sum(croppedImage)[0]);
				counter++;
			}
		}

		cv::normalize(out_img, out_img, 0, 1, NORM_MINMAX);
		out_img.convertTo(out_img, CV_64FC1);
		Mat sinogramm = out_img;
		cv::normalize(sinogramm, sinogramm, 0, 1, NORM_MINMAX);
		cv::imshow("sinogramm", sinogramm);

		
		//Make 1 row from sinogramm
		sinogramm = sinogramm.reshape(1, 1);
		


		//--------------------MULTFILM WITH LINES-------------------------------

		//Mat mult = Mat::zeros(imagesrc.size(), CV_64FC1);
		//for (int k = 0; k < sino_heigih * d * imagesrc.rows * imagesrc.cols; k+= (imagesrc.cols* imagesrc.rows))
		//{

		//	for (int i = 0; i < croppedImage.rows; i++)
		//	{
		//		for (int j = 0; j < croppedImage.cols; j++)
		//		{
		//			;	
		//			mult.at<double>(i,j) = tempmat.at<double>(0, k  + (j * croppedImage.rows + i));
		//		}
		//	}

		//cout << k << endl;
		//cv::imshow("mult", mult);
		//waitKey(5);
		//}


		//==== Preparing data for Kazf method.  IS IT RIGHT WAY?
		cv::normalize(sinogramm, sinogramm, 0, 1, cv::NormTypes::NORM_MINMAX);
		cv::normalize(matLines1d, matLines1d, 0, 1, cv::NormTypes::NORM_MINMAX);

		//===== Creation dynamic array for vector solution
		double* vecSol1d = new double[imagesrc.cols * imagesrc.rows];

		// Using Kazf method
		Mkazf(matLines1d.ptr<double>(), sinogramm.ptr<double>(), vecSol1d, imagesrc.cols * imagesrc.rows, sinogramm.cols);

		//Creation vector from our dynamic array
		vector<double> vecSol(imagesrc.cols* imagesrc.rows);
		for (int i = 0; i < vecSol.size(); i++)
		{
			vecSol[i] = vecSol1d[i];
		}

		

		// 2 ways to out our image: via "memcpy" or "reshape"

		Mat recoveryImage = Mat(imagesrc.rows, imagesrc.cols, CV_64FC1);
		std::memcpy(recoveryImage.data, vecSol.data(), vecSol.size() * sizeof(double));	
		cv::normalize(recoveryImage, recoveryImage, 0, 1, cv::NormTypes::NORM_MINMAX);
		cv::imshow("recovery", recoveryImage);



		Mat testMat = Mat(vecSol).reshape(1, imagesrc.rows);
		cv::normalize(testMat, testMat, 0, 1, cv::NormTypes::NORM_MINMAX);
		testMat.convertTo(testMat, CV_64FC1);
		cv::imshow("testMat", testMat);


	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	else {


		vector<vector<double>> koeffs;
		koeffs.resize(3);
		for (int i = 0; i < koeffs.size(); i++)
		{
			koeffs[i].resize(3);
		}

		koeffs[0][0] = 3;
		koeffs[0][1] = 5;
		koeffs[0][2] = 4;
		koeffs[1][0] = 8;
		koeffs[1][1] = 2;
		koeffs[1][2] = 7;
		koeffs[2][0] = 1;
		koeffs[2][1] = 1;
		koeffs[2][2] = 1;

		cout << koeffs[0][0] << " " << koeffs[0][1] << " " << koeffs[0][2] << endl;
		cout << koeffs[1][0] << " " << koeffs[1][1] << " " << koeffs[1][2] << endl;
		cout << koeffs[2][0] << " " << koeffs[2][1] << " " << koeffs[2][2] << endl;




		double* koeffs1d = new double[koeffs.size() * koeffs[0].size()];

		for (int i = 0; i < koeffs.size(); i++)
		{
			for (int j = 0; j < koeffs[0].size(); j++)
			{
				koeffs1d[i * koeffs.size() + j] = koeffs[i][j];
			}
		}



		vector<double> from1d(9);
		for (int i = 0; i < 9; i++)
		{
			from1d[i] = koeffs1d[i];
		}


		//vector<double> svob{ 10,20,24 };
		//vector<double> vecSol(3);

		double svob[] = { 17,19, 4 };
		double vecSol[] = { 0, 0, 0 };


		Mkazf(koeffs1d, svob, vecSol, 3, 3);


		cout << "\n";

		for (int i = 0; i < 3; i++)
		{
			cout << "vecSol  " << vecSol[i] << endl;
		}


		from1d.resize(9);

		for (int i = 0; i < 3; i++)
		{
			from1d[i] = koeffs1d[i];
		}
		from1d.push_back(0);


		Mat koeffsMat = Mat::zeros(3, 3, CV_64FC1);

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				koeffsMat.at<double>(i, j) = from1d[i * 3 + j];
			}
		}


		imshow("koeffsMat", koeffsMat);



		double* MatTo1d = new double[koeffsMat.rows * koeffsMat.cols];

		for (int i = 0; i < koeffsMat.rows; i++)
		{
			for (int j = 0; j < koeffsMat.cols; j++)
			{
				MatTo1d[j * koeffsMat.rows + i] = koeffsMat.at<double>(i, j);
			}
		}

		for (int i = 0; i < 9; i++)
		{
			cout << MatTo1d[i] << endl;
		}



		int a = 1;
	}


	waitKey(0);
	return 0;
}


