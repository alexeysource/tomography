﻿#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <omp.h>



using namespace cv;
using namespace std;


///////////----1ST TASK Tomography

//---MAKE LINE FOR S AND THETTA
Mat computeLineRot(Mat imgzoom, int S, int thetta, int centerPov)
{	
	imgzoom = Mat::zeros(imgzoom.size(), CV_64FC1);	
	line(imgzoom, Point(0, S), Point(imgzoom.cols, S), 1);
	Mat rot = getRotationMatrix2D(Point(centerPov, centerPov), thetta + 90, 1);
	warpAffine(imgzoom, imgzoom, rot, imgzoom.size());
	return imgzoom;
}



Mat MatSwap(Mat mat)
{
	Mat matSwap = Mat::zeros(mat.size(), CV_64FC1);
	mat.copyTo(matSwap);
	Mat ql2(matSwap, Rect(0, 0, matSwap.cols / 2, matSwap.rows));
	Mat qr2(matSwap, Rect(matSwap.cols / 2, 0, matSwap.cols / 2, matSwap.rows));
	Mat tmp2;
	ql2.copyTo(tmp2);
	qr2.copyTo(ql2);
	tmp2.copyTo(qr2);
	return matSwap;
}



void obrProets(Mat sinogramm)
{


	std::cout << "obrProets staretd" << endl;
	Mat MatForRotation = Mat::zeros(sinogramm.cols, sinogramm.cols, CV_64FC1);
	Mat MatResult = Mat::zeros(MatForRotation.size(), CV_64FC1);
	Mat MatForSum = Mat::zeros(MatForRotation.size(), CV_64FC1);
	int CenterX = MatForRotation.cols / 2;
	int CenterY = MatForRotation.rows / 2;

	int row = 0;


	for (int row = 0; row < 180; row++)
	{
		
			for (int i = 0; i < MatForRotation.rows; i++)
			{
				for (int j = 0; j < MatForRotation.cols; j++)
				{
					MatForRotation.at<double>(i, j) = sinogramm.at<double>(row, j);
				}
			}
			
			cv::normalize(MatForRotation, MatForRotation, 0, 1, NORM_MINMAX);
			Mat rot = getRotationMatrix2D(Point(CenterX, CenterY), row, 1);
			warpAffine(MatForRotation, MatForSum, rot, MatForSum.size());
			cv::normalize(MatForSum, MatForSum, 0, 1, NORM_MINMAX);

			
			MatResult += MatForSum;	
			
			//cv::imshow("MatResult", MatResult);
			//cv::imshow("MatForSum", MatForSum);
			//waitKey(100);

		
	}


	//MatResult = MatForRotation + MatForSum;
	cv::normalize(MatResult, MatResult, 0, 1, NORM_MINMAX);
	cv::imshow("MatResult", MatResult);

	std::cout << "obrProets done" << endl;

}



int main()
{
	std::cout << " Enter 0 for tranformation, or other for test " << endl;
	int p = 0;
	cin >> p;

	if (p == 0)
	{

		//Mat imagesrc = Mat::zeros(50, 50, CV_64FC1);

		Mat imagesrc = imread("Star75.png", IMREAD_GRAYSCALE);
		

		//rectangle(imagesrc, Rect(30, 30, 40, 40), Scalar(255), -1);
		//rectangle(imagesrc, Rect(10, 15, 30, 20), Scalar(255), -1);
		//rectangle(imagesrc, Rect(0, 0, 25, 50), Scalar(255), -1);
		//rectangle(imagesrc, Rect(0, 0, 50, 50), Scalar(255), -1);


		Mat imageConverted;
		imagesrc.convertTo(imageConverted, CV_64FC1);
		cv::normalize(imageConverted, imageConverted, 0, 1, NORM_MINMAX);

		cv::imshow("imageConverted", imageConverted);
		
		int step_thetta = 1;
		int sino_heigih = 180 / step_thetta; // Sinigram's height depends from thetta step


		double w = imagesrc.cols;
		double h = imagesrc.rows;
		std::cout << "Image src w, h:  " << w << "x" << h << endl;

		int d = int(sqrt(w * w + h * h));	
		int dw = int((d - w) / 2);
		int dh = int((d - h) / 2);

		Mat imagezero = Mat::zeros(imagesrc.size(), CV_64FC1);


		Mat zoom_img;
		
		cv::copyMakeBorder(imagezero, zoom_img, dh, dh, dw, dw, BORDER_CONSTANT);
		Mat imgzoom2= Mat::zeros(zoom_img.size(), CV_64FC1);
		int centerPov = imgzoom2.cols / 2;
		Mat out_img = Mat::zeros(sino_heigih, d, CV_64FC1);
		Mat Line;
		Mat croppedImage = Mat::zeros(imagesrc.size(), CV_64FC1);	
		int counter = 0;		
		Mat matLines1d = Mat::zeros(1, sino_heigih * d* imagesrc.rows * imagesrc.cols, CV_64FC1);
		


#pragma omp parallel for
		for (int thetta = 0; thetta < sino_heigih; thetta += step_thetta)
		{
			for (int S = 0; S < d; S++)
			{
				//Creation picture with line and "mul" it to src_img
				Line = computeLineRot(imgzoom2, S, thetta, centerPov);
				croppedImage = Line(Rect(dw, dh, w, h));
				
				normalize(croppedImage, croppedImage, 0, 1, NORM_MINMAX); //1sek
				croppedImage = croppedImage.mul(imageConverted);
						
				out_img.at<double>(thetta, S) = double(sum(croppedImage)[0]);
				counter++;
			}
		}



		//cv::normalize(out_img, out_img, 0, 1, NORM_MINMAX);
		out_img.convertTo(out_img, CV_64FC1);


		Mat sinogramm = out_img;
		cv::normalize(sinogramm, sinogramm, 0, 1, NORM_MINMAX);
		cv::imshow("sinogramm", sinogramm);
		cout << "Sinogramm Rows x Cols   " << sinogramm.rows << "x" << sinogramm.cols << endl;
		
		Mat sinogrammSwap = Mat::zeros(sinogramm.size(), CV_64FC1);
		sinogrammSwap = MatSwap(sinogramm);


		Mat Filter = Mat::zeros(sinogramm.size(), CV_64FC1);
		for (int i = 0; i < Filter.rows; i++)
		{
			for (int j = 0; j < Filter.cols; j++)
			{
				double a = j - d / 2;
				double x = fabs(a);
				Filter.at<double>(i, j) = x;
			}
				
		}
		
		//cv::normalize(Filter, Filter, 0, 1, cv::NormTypes::NORM_MINMAX);
		cv::imshow("Filter", Filter);
		Mat Filter2 = Mat::zeros(sinogramm.size(), CV_64FC1);


		Mat img2ch = Mat::zeros(sinogrammSwap.size(), CV_64FC2);
	
		for (int i = 0; i < sinogrammSwap.rows; i++)
		{
			for (int j = 0; j < sinogrammSwap.cols; j++)
			{
				img2ch.at<Vec2d>(i, j)[0] = sinogrammSwap.at<double>(i, j);
				img2ch.at<Vec2d>(i, j)[1] = 0;
			}
		}

		Mat dftsinogramm = Mat::zeros(sinogrammSwap.size(), CV_64FC2);

		cv::dft(img2ch, dftsinogramm, cv::DFT_ROWS | cv::DFT_COMPLEX_OUTPUT);

		dftsinogramm = MatSwap(dftsinogramm);

		for (int i = 0; i < dftsinogramm.rows; i++)
		{
			for (int j = 0; j < dftsinogramm.cols; j++)
			{
				dftsinogramm.at<Vec2d>(i, j)[0] = dftsinogramm.at<Vec2d>(i, j)[0] * Filter.at<double>(i, j);
				dftsinogramm.at<Vec2d>(i, j)[1] = dftsinogramm.at<Vec2d>(i, j)[1] * Filter.at<double>(i, j);
			}
		}

		

		dftsinogramm = MatSwap(dftsinogramm);

		Mat dft = Mat::zeros(sinogrammSwap.size(), CV_64FC1);
		for (int i = 0; i < dftsinogramm.rows; i++)
		{
			for (int j = 0; j < dftsinogramm.cols; j++)
			{
				
				dft.at<double>(i, j) = sqrt(dftsinogramm.at<Vec2d>(i, j)[0] * dftsinogramm.at<Vec2d>(i, j)[0] + dftsinogramm.at<Vec2d>(i, j)[1] * dftsinogramm.at<Vec2d>(i, j)[1]);
			}
		}
		dft += Scalar::all(1);
		cv::log(dft, dft);
		dft = dft(Rect(0, 0, dft.cols & -2, dft.rows & -2));
		cv::normalize(dft, dft, 0, 1, NORM_MINMAX);
		cv::imshow("DFTSinogrammSwapoedFiltered", dft);




		//cv::normalize(dftsinogramm, dftsinogramm, 0, 1, NORM_MINMAX);

		Mat idftcomplex = Mat::zeros(dftsinogramm.size(), CV_64FC2);
		cv::idft(dftsinogramm, idftcomplex, cv::DFT_ROWS | cv::DFT_COMPLEX_OUTPUT);

		Mat idftModul = Mat::zeros(dftsinogramm.size(), CV_64FC1);
 
		for (int i = 0; i < idftModul.rows; i++)
		{
			for (int j = 0; j < idftModul.cols; j++)
			{
				//idftModul.at<double>(i, j) = sqrt(idftcomplex.at<Vec2d>(i, j)[0] * idftcomplex.at<Vec2d>(i, j)[0] + idftcomplex.at<Vec2d>(i, j)[1] * idftcomplex.at<Vec2d>(i, j)[1]);				
				idftModul.at<double>(i, j) = idftcomplex.at<Vec2d>(i, j)[0];				

			}
		}
		//cv::normalize(idftModul, idftModul, 0, 1, NORM_MINMAX);
		
		cout << idftModul.rows << " " << idftModul.cols << endl;

	

		//Mat idftModulcropped = Mat::zeros(sinogrammSwap.size(), CV_64FC2);

		//idftModul = idftModul(Rect(1, 0, idftModul.cols - 2, idftModul.rows));
		cout << idftModul.rows << " " << idftModul.cols << endl;



		
		//idftModul = idftModul(Rect(1, 0, idftModul.cols - 2, idftModul.rows));
		idftModul = MatSwap(idftModul);

		cv::normalize(idftModul, idftModul, 0, 1, NORM_MINMAX);

		cv::imshow("idftModul", idftModul);



		obrProets(idftModul);

		



	}

	//////////////TEST//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	else {





	Mat imagesrc = imread("Lenna256.png", IMREAD_GRAYSCALE);


	Mat imageConverted;
	imagesrc.convertTo(imageConverted, CV_64FC1);
	cv::normalize(imageConverted, imageConverted, 0, 1, NORM_MINMAX);

	Mat imageblur = Mat(imageConverted.rows, imageConverted.cols, CV_64FC1);

	cv::GaussianBlur(imageConverted, imageblur, cv::Size(0, 0), 3);
	cv::addWeighted(imageConverted, 1.5, imageblur, -0.5, 0, imageConverted);

	cv::imshow("imageConverted", imageConverted);



	}
	waitKey(0);
	return 0;
}


